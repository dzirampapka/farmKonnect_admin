import React, { useState } from 'react';
import {Button, Modal, Dropdown, Container, Row, Col} from 'react-bootstrap';


export function AdminActionConfirmationModal (props) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    return (
        <>
            <Dropdown.Item onClick={handleShow}> {props.action_title} </Dropdown.Item>

            <Modal
                show={show}
                onHide={handleClose}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{props.action_title + " "} {props.row_data.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col xs={12} md={12}>
                               Are you sure you want to {props.action_title} this user?
                                <p><b>{props.row_data.name}</b> will get an email notification on this action</p>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => props.action()}>
                        {props.action_title}
                    </Button>
                    <Button variant="danger" onClick={handleClose}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
