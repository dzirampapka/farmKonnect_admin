import React, {Component} from "react";
import {
    Button,
    Card,
    ListGroup,
    ListGroupItem,
    Nav,
    CardGroup,
    CardDeck,
    CardColumns,
    Badge,
    Row, Col, Container
} from "react-bootstrap";
import {api} from "../../config/apiList";
import {InPageLoader} from "../../../_metronic/layout/components/loader/inPageLoader";
import {apiAction} from "../../helpers/apiConnection";


class ViewUserDetailsPage extends Component {

    //initial state
    state = {
        data: [],
        loader: false
    };

    componentDidMount() {


        // let user_id = this.props.match.params.club_id;

        /* //display loader
         this.setState({
             loader: true,
         });

         let editedApi = {...api.club.specific_club_members};
         editedApi.url = editedApi.url + user_id;

         apiAction(editedApi, this.init_data);
 */
    }

    init_data = (res) => {

        let user_data = [];

        if (res.data.hasOwnProperty("data")) {
            if (res.data.data !== "") {
                user_data = res.data.data.map((element, index) => {
                    let club_name = {club_name: element.club.name};
                    return {...element.user, ...club_name};
                });
            }

        }


        if (user_data.length !== 0) {
            this.setState({
                data: user_data,
                loader: false,
            })
        }
    };


    render() {
        return (
            <>
                <Row>
                    <Col xs={12} md={12}>
                        <div className="mb-5">
                            <Card>
                                <Card.Body>
                                    <div className="text-center">
                                        <b className="h3">AFFILIATE VALUE</b>
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                    </Col>
                    <Col xs={12} md={6}>
                        <div className="mt-4">
                            <Card border="secondary" bg="secondary">
                                <div className="text-center">
                                    <Card.Header>
                                        <b className="h4">AFFILIATES</b>
                                    </Card.Header>
                                </div>

                                <Card.Body>
                                    <ListGroup>
                                        <ListGroup.Item>
                                            <b>NO OF AFFILIATES:</b>
                                            <b>
                                            <span className="float-right">
                                                <Badge pill variant="dark">
                                                    10
                                                 </Badge>
                                            </span>
                                            </b>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            <b>AFFILIATE VALUE:</b>
                                            <b>
                                            <span className="float-right">
                                                <Badge pill variant="dark">
                                                    10
                                                 </Badge>
                                            </span>
                                            </b>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            <b>PERCENTAGE:</b>
                                            <b>
                                            <span className="float-right">
                                                <Badge pill variant="dark">
                                                    10
                                                 </Badge>
                                            </span>
                                            </b>
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </div>
                    </Col>
                    <Col xs={12} md={6}>
                        <div className="mt-4">
                            <Card bg="secondary">
                                <div className="text-center">
                                    <Card.Header>
                                        <b className="h4">CLIENT</b>
                                    </Card.Header>
                                </div>

                                <Card.Body>
                                    <ListGroup>
                                        <ListGroup.Item>
                                            <b>NO OF CLIENTS:</b>
                                            <b>
                                            <span className="float-right">
                                                <Badge pill variant="dark">
                                                    10
                                                 </Badge>
                                            </span>
                                            </b>
                                        </ListGroup.Item>

                                        <ListGroup.Item>
                                            <b>CLIENT VALUE:</b>
                                            <b>
                                            <span className="float-right">
                                                <Badge pill variant="dark">
                                                    10
                                                 </Badge>
                                            </span>
                                            </b>
                                        </ListGroup.Item>

                                        <ListGroup.Item>
                                            <b>PERCENTAGE:</b>
                                            <b>
                                            <span className="float-right">
                                                <Badge pill variant="dark">
                                                    10
                                                 </Badge>
                                            </span>
                                            </b>
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </div>
                    </Col>
                </Row>
            </>
        );
    }
}

export default ViewUserDetailsPage;
