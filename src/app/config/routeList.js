export const routeList = Object.freeze({

    referrals: {
        view: "/view_all_affiliate",
        view_active: "/view_active_affiliate",
        view_verified: "/view_verified_affiliate",
        view_suspended: "/view_suspended_affiliate",
        view_user_details: "/affiliate_value",
    },
    club: {
        view_club_members: "/view_club_members",
        view_clubs: "/view_clubs",
        update_club: "/update_club",
        add_club: "/add_club",
        update_club_logo: "/update_club_logo",
        add_club: "/add_club",
    },
    settings: {
        profile: "/profile",
        view_clubs: "/view_clubs",
    }
});