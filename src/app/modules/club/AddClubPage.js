import React, {Component} from "react";

import {apiAction} from "../../helpers/apiConnection";
import {api} from "../../config/apiList";
import {routeList} from "../../config/routeList";
import {generic_submit, notify} from "../../helpers/genericFunctions";
import {
    Card,
    Spinner
} from "react-bootstrap";

class AddClubPage extends Component {

    render() {
        let handleSubmit = (e) => {
            e.preventDefault();
            notify("Please wait", "default");


            let add_club_api = {...api.club.add_club};

            generic_submit(club_call_back_fn, "add_club", add_club_api);
        };
        let club_call_back_fn = (res) => {
            console.log(res);
            if (res.status === 200) {
                // this.props.history.push(routeList.club.view_clubs);
                notify("Club added successfully", "");
            }
        };


        return (
            <div className="page-container mt-3">
                <div className="main-content">
                    <div className="row justify-content-md-center">

                        <div className="col-md-7 mb-5">
                            <div className="card">
                                <div className="card-body">
                                    <div className="text-center mb-10 mb-lg-20">
                                        <h3 className="font-size-h1">ADD CLUB</h3>
                                        <p className="text-muted font-weight-bold">
                                            Enter Club Details
                                        </p>
                                    </div>


                                    <form
                                        id="add_club"
                                        onSubmit={handleSubmit}
                                        encType="multipart/form-data"
                                    >
                                        <div className="form-group">
                                            <label
                                                className="font-weight-semibold"
                                                htmlFor="userName"
                                            >
                                                Club Name:
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="name"
                                                placeholder="Club Name"
                                                required

                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Member Limit:
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="member_limit"
                                                placeholder="Number of Members Allowed"
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Referral Target:
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="club_referral_target"
                                                placeholder="Number of Members Allowed"
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Client Target:
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="club_client_target"
                                                placeholder="Number of Members Allowed"
                                                required
                                            />
                                        </div>
                                        <div className="form-group  mt-5">
                                            <label
                                                className="font-weight-semibold"
                                                htmlFor="confirmPassword"
                                            >
                                                Club Logo
                                            </label>
                                            <input
                                                type="file"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                id="club_logo"
                                                name="image"
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Club Requirements:
                                            </label>
                                            <textarea
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="requirement_details"
                                                placeholder="Enter Club requirements"
                                                required
                                            >


                                            </textarea>
                                        </div>
                                        <div className="form-group">
                                            <div className="text-center p-t-15">
                                                <button className="btn btn-rounded btn-dark">
                                                    Add Club
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddClubPage;
