const admin_headers = {
    // 'Content-Type': 'application/json',
    // 'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Type': 'multipart/form-data',
    // 'authorization': JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")).accessToken : null,



};

// let baseUrl = "https://farmkonnect-backend.herokuapp.com/";
let baseUrl = "http://127.0.0.1:9900/";


export const api = Object.freeze({

    auth: {
        signup: {
            url: baseUrl + "user/register",
            method: "post",
            data: "",
            headers: admin_headers,
        },
        login: {
            url: baseUrl + "login",
            method: "post",
            data: "",
            headers: admin_headers,
        },
        compare_result: {
            url: baseUrl + "compare_result",
            method: "post",
            data: "",
            headers: admin_headers,
        }
    },
    club: {
        all_club_members: {
            url: baseUrl + "admin/club/members",
            method: "get",
            data: "",
            headers: admin_headers,
        },
        specific_club_members: {
            url: baseUrl + "admin/club/members/", // pass in :club_id in url
            method: "get",
            data: "",
            headers: admin_headers,
        },
        all_clubs: {
            url: baseUrl + "admin/clubs",
            method: "get",
            data: "",
            headers: admin_headers,
        },
        get_club: {
            url: baseUrl + "admin/club/get/",  // pass in :club_id in url
            method: "get",
            data: "",
            headers: admin_headers,
        },
        update_club: {
            url: baseUrl + "admin/club/update/",    // pass in :club_id in url
            method: "put",
            data: "",
            headers: admin_headers,
        },
        update_club_logo: {
            url: baseUrl + "admin/club/logo/",  // pass in :club_id in url
            method: "put",
            data: "",
            headers: admin_headers,
        },
        add_club: {
            url: baseUrl + "admin/club/add",
            method: "post",
            data: "",
            headers: admin_headers,
        },
    },
    affiliates: {
        all_affiliates: {
            url: baseUrl + "admin/users",
            method: "get",
            data: "",
            headers: admin_headers,
        },
        active_affiliates: {
            url: baseUrl + "admin/users/active",
            method: "get",
            data: "",
            headers: admin_headers,
        },
        suspended_affiliates: {
            url: baseUrl + "admin/users/suspended",
            method: "get",
            data: "",
            headers: admin_headers,
        },
        verified_affiliates: {
            url: baseUrl + "admin/users/verified",
            method: "get",
            data: "",
            headers: admin_headers,
        },
    },
    admin_actions: {

        view_affiliate: {
            url: baseUrl + "admin/user",
            method: "get",
            data: "",
            headers: admin_headers,
        },suspend_affiliate: {
            url: baseUrl + "admin/user/suspend/", //user_id to be added
            method: "put",
            data: "",
            headers: admin_headers,
        },
        unsuspend_affiliate: {
            url: baseUrl + "admin/user/unsuspend/", //user_id to be added
            method: "put",
            data: "",
            headers: admin_headers,
        },

        verify_affiliate: {
            url: baseUrl + "admin/user/verify/", //user_id to be added
            method: "put",
            data: "",
            headers: admin_headers,
        },
        unverify_affiliate: {
            url: baseUrl + "admin/user/unverify/", //user_id to be added
            method: "put",
            data: "",
            headers: admin_headers,
        },
    },
    settings: {
        profile: {
            url: baseUrl + "admin/",//admin id in front
            method: "get",
            data: "",
            headers: admin_headers,
        }
    }
});
