import React, {Component} from "react";

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import {apiAction} from "../../helpers/apiConnection";
import {api} from "../../config/apiList";
import ViewUsersPage from "../users/ViewUsersPage";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class ViewAffiliatePage extends Component {

    constructor(props) {
        super(props);
        this.init_data = this.init_data.bind(this)
    }

    //initial state
    state = {
        data: [],
        loader: false,
        status: null
    };

    componentDidMount() {

        //display loader
        this.setState({ loader: true });

        apiAction(api.affiliates.verified_affiliates, this.init_data);

    }

    //notifications
    notify = (msg, type) => {toast[type](msg)};
    dismiss = () => toast.dismiss();


    init_data = (res) => {
        let user_data = "";

        if (res.hasOwnProperty("data")) {

            user_data = res.data.data.map((element, index) => {
                let club_name = {club_name: element.club.name};
                return {...element.user, ...club_name};
            });

        } else {

            user_data = res.filter((element, index) => {

                let club_name_and_user_data = "";

                //select active users....this is done to immediately
                //remove changed status
                if (parseInt(element.status) === 2) {
                    club_name_and_user_data = {...element};
                }
                return club_name_and_user_data
            });

        }

        if (user_data.length !== 0) {
            this.setState({
                data: user_data,
                loader: false,
            })
        } else {
            this.setState({
                data: [],
                loader: false,
            })
        }
    };

    render() {
        let loading_notification = (notify) => {
            return (
                <div>
                    <ToastContainer
                        position="top-center"
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        autoClose={false}
                    />
                </div>
            );
        };

        return (
            <div>
                {loading_notification(this.notify)}

                <ViewUsersPage
                    update_status={this.init_data}
                    status={this.state.status}
                    users={this.state.data}
                    loader={this.state.loader}
                    table_title="Verified Affiliates"/>
            </div>
        );
    }
}

export default ViewAffiliatePage;
