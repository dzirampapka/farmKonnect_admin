import React from "react";
import { apiAction } from "./apiConnection";
import axios from "axios";


//notification
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css"; // optional styles


export const populate_options = ((options, name, text_as_value = "") => {
    if (Array.isArray(options)) {
        return options.map(element => {

            let id = element.id;

            if (text_as_value !== "") {
                id = element[text_as_value];
            }
            return (
                <option value={id} key={element.id}>
                    {element[name]}
                </option>
            )
        })
    }
});

export const generic_submit = ((call_back_fn, form_name, api) => {


    let form = document.getElementById(form_name);

    let formData = new FormData(form);

    let editedApi = { ...api };
    editedApi.data = formData;

    apiAction(editedApi, call_back_fn);

});


export const submit = ((call_back_fn, form_name, api) => {



    try {
       
        let form = document.getElementById(form_name);

        //check if id element exists
        if(typeof(form) == 'undefined' || form == null){
            throw 'Invalid id, element does not exist!';
        }

        //check if id is a valid form element
        if(form.nodeName != 'FORM'){
            throw 'Id element is not a form!';
        }

        //check if api is an object
        if(typeof(api) != 'object'){
            throw "api is not an object";
        }


        //check if api method has required obj keys
        if(!api.hasOwnProperty('url')){
            throw "url property is required in api object";
        }
        if(!api.hasOwnProperty('method')){
            throw "method property is required in api object";
        }
        if(api.method == ""){
            throw "api method is empty in object";
        }

    
        //check if api url is a valid http string
       
        if(api.url == ""){
            throw "api url property is empty in object";
        }

        if(!isValidHttpUrl(api.url)){
            throw "api url property is not a valid url";
        }
       


        //get form fields
        let formData = new FormData(form);

        let edit_api = { ...api };


        //append form data to data field
        edit_api.data = formData;

        //send form to destination
        axios({
            method: edit_api.method,
            url: edit_api.url,
            headers: edit_api.headers,
            data: edit_api.data,
        }).catch(function (error) {
            // handle error
            console.log(error);
        }).then(res => {
            call_back_fn(res)
        })

    } catch (e) {
        console.log(e)
    }

});

function isValidHttpUrl(string) {
    let url;
    
    try {
      url = new URL(string);
    } catch (_) {
      return false;  
    }
  
    return url.protocol === "http:" || url.protocol === "https:";
  }




//function returns form field as an object...name as key and input field content as value
//this function does not return file input field, handle that separately

export const getFormFields = (form => {
    let formFields = {};
    for (let i = 0; i < form.length; i++) {
        formFields[form[i].name] = form[i].value;
    }
    return formFields;
});

export const notify = (msg, text_variant = "text-dark", duration = 2000) => {


    return (toaster.notify(
        <div className={"h3 " + text_variant}>
            {msg}
        </div>, {
        duration: duration
    })
    )
};

export const myOnClick = (e => {
    return 0;
    // e.preventDefault();
});