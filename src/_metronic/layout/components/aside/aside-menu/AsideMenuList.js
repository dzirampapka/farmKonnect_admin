/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { routeList } from "../../../../../app/config/routeList";

import { AsideMenuSection } from "./AsideMenuSections";

export function AsideMenuList({ layoutProps }) {
    return (
        <>
            {/* begin::Menu Nav && Side nav */}
            <ul className={`menu-nav ${layoutProps.ulClasses}`}>
                {/* Affiliates section */}
                <AsideMenuSection
                    // sectionHeader={"Affiliate"}
                    sectionRoute={"/referrals"}
                    sectionTitle={"Affiliates"}
                    sectionName={"Referrals"}
                    sectionSVG={`/media/svg/icons/Design/Cap-2.svg`}
                    sectionLevels={[
                        {
                            id: 1,
                            route: routeList.referrals.view,
                            levelName: "All Affiliates",
                        },
                        {
                            id: 2,
                            route: routeList.referrals.view_verified,
                            levelName: "Verified Affiliates",
                        },
                        {
                            id: 3,
                            route: routeList.referrals.view_active,
                            levelName: "Activated Affiliates",
                        },
                        {
                            id: 4,
                            route: routeList.referrals.view_suspended,
                            levelName: "Suspended Affiliates",
                        },
                    ]}
                />

                <AsideMenuSection
                    // sectionHeader={"Clubs"}
                    sectionRoute={"/clients"}
                    sectionTitle={"clubs"}
                    sectionName={"Clubs"}
                    sectionSVG={`/media/svg/icons/Design/Cap-2.svg`}
                    sectionLevels={[
                        {
                            id: 5,
                            route: routeList.club.view_clubs,
                            levelName: "Club Category",
                        },
                        {
                            id: 6,
                            route: routeList.club.add_club,
                            levelName: "Add Club",
                        },
                    ]}
                />


                {/* Chat section */}
                <AsideMenuSection
                    // sectionHeader={"Chat"}
                    sectionRoute={"/chats"}
                    sectionTitle={"chats"}
                    sectionName={"Chat"}
                    sectionSVG={`/media/svg/icons/Design/Cap-2.svg`}
                    sectionLevels={[
                        {
                            id: 7,
                            route: "/google-material/inputs",
                            levelName: "Chat Request",
                        },
                        {
                            id: 8,
                            route: "/google-material/inputs",
                            levelName: "Chat History",
                        },
                    ]}
                />

                {/* Support section */}
                <AsideMenuSection
                    // sectionHeader={"Support"}
                    sectionRoute={"/support"}
                    sectionTitle={"support"}
                    sectionName={"Support"}
                    sectionSVG={`/media/svg/icons/Design/Cap-2.svg`}
                    sectionLevels={[
                        {
                            id: 9,
                            route: "/google-material/inputs",
                            levelName: "Chat Request",
                        },
                        {
                            id: 10,
                            route: "/google-material/inputs",
                            levelName: "Chat History",
                        },
                    ]}
                />
                <AsideMenuSection
                    sectionHeader={"Settings"}
                    sectionRoute={"/settings"}
                    sectionTitle={"settings"}
                    sectionName={"Settings"}
                    sectionSVG={`/media/svg/icons/Design/Cap-2.svg`}
                    sectionLevels={[
                        {
                            id: 9,
                            route: routeList.settings.profile,
                            levelName: "Profile",
                        }
                    ]}
                />
            </ul>
        </>
    );
}
