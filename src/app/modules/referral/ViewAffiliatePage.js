import React, {Component} from "react";

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import {apiAction} from "../../helpers/apiConnection";
import {api} from "../../config/apiList";
import ViewUsersPage from "../users/ViewUsersPage";


class ViewAffiliatePage extends Component {

    constructor(props) {
        super(props);
        this.update_status = this.update_status.bind(this)
    }

    //to update state from child class component
    update_status(data, random_figure_for_update) {
        this.setState({
            data: data,
            status: Math.random()
        })
    }

    //initial state
    state = {
        data: [],
        loader: false,
        status: null
    };

    componentDidMount() {

        //display loader
        this.setState({loader: true});

        apiAction(api.affiliates.all_affiliates, this.init_data);
    }

    init_data = (res) => {
        let user_data = "";

        if (res.hasOwnProperty("data")) {

            user_data = res.data.data.map((element, index) => {
                let club_name = {club_name: element.club.name};
                return {...element.user, ...club_name};
            });

        } else {

            user_data = res.filter((element, index) => {

                let club_name_and_user_data = "";

                //select active users....this is done to immediately
                //remove changed status
                if (parseInt(element.status) === 1) {
                    club_name_and_user_data = {...element};
                }
                return club_name_and_user_data
            });

        }

        // console.log(user_data);
        // return;

        if (user_data.length !== 0) {
            this.setState({
                data: user_data,
                loader: false,
            })
        } else {
            this.setState({
                data: [],
                loader: false,
            })
        }
    };



    render() {


        return (
            <div>

                <ViewUsersPage
                    update_status = {this.update_status}
                    status = {this.state.status}
                    users={this.state.data}
                    loader={this.state.loader}
                    table_title="All Affiliates"/>
            </div>
        );
    }
}

export default ViewAffiliatePage;
