import React, {Component} from "react";

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import {InPageLoader} from "../../../_metronic/layout/components/loader/inPageLoader";
import {apiAction} from "../../helpers/apiConnection";
import {api} from "../../config/apiList";
import {routeList} from "../../config/routeList";
import {Dropdown} from "react-bootstrap";
import {ViewClubDetailsModal} from "./ViewClubDetailsModal"
import {Link} from "react-router-dom";

class AddClientPage extends Component {
    state = {

        //notifications for user
        loader: true,
        message: {
            error: false,
            message: "",
            data: ""
        },
        message1: "test",

        //search params
        search: false,
        data: [],
        filter_data: [],
    };


    componentDidMount() {
        //get document types for form options
        apiAction(api.club.all_clubs, this.init_data);
    }

    init_data = (res) => {
         console.log(res.data.data);

        if (parseInt(res.request.status) === 200) {

            let user_data = res.data.data;
            this.setState({
                    data: user_data,
                    loader: false,
                }, () => this.setState({
                    filter_data: this.state.data
                })
            )
            return;
        }

        //check if server responds(eg lack of internet connection/empty or no server response)
        if (parseInt(res.request.status) === 0) {
            this.setState({
                message: {
                    error: true,
                    message: "",
                    data: "Network error, please check your internet connection"
                },
            });
        }
    };

    handle_submit = (event) => {
        let input_value = document.querySelector("#inlineFormInputGroup").value;
        this.search_inventory(input_value);
    };

    search_inventory = (input) => {
        // console.log(input);
        let filtered_inventory = this.state.data.filter((element) => {
            let result = element.name.toLowerCase().search(input.toLowerCase().trim());
            return result !== -1;
        });
        // console.log(filtered_inventory);

        this.setState({
            filter_data: filtered_inventory
        });


    };

    render() {

        return (
            <div className="page-container mt-3">
                <div className="main-content">


                    {/*search*/}
                    <div className="row justify-content-md-center mb-2">
                        <div className="col-md-6 col-lg-8">
                            <div className="card">
                                <div className="card-body">
                                    <form>

                                        <div className="input-group mb-2">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <i className="fab fa-searchengin h1"></i>
                                                </div>
                                            </div>
                                            <input type="text"
                                                   className="form-control form-control-solid h-auto py-5 px-6"
                                                   id="inlineFormInputGroup"
                                                   onInput={(e) => this.handle_submit(e)} placeholder="Search Clubs"/>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    {/*clubs display*/}
                    {this.state.loader ? (

                        <div className="login-form login-signin" style={{display: "block"}}>
                            <div className="text-center mb-5 mb-lg-10">
                                <InPageLoader loader={this.state.loader}/>
                                <h5 className="text-danger text-center">{this.state.message.data}</h5>
                            </div>
                        </div>

                    ) : (
                        <div className="row justify-content-md-center">
                            <div className="col-md-6 col-lg-8">
                                {
                                    this.state.filter_data.map((element) => {
                                        return (
                                            <div className="card shadow mb-2" key={element.id}>
                                                <div className="card-body p-3">
                                                    <div className="row">
                                                        <div className="col-2">
                                                            <img alt="" src={element.image}
                                                                 className=" rounded product_thumbnail shadow"/>
                                                        </div>
                                                        <div className="col-6 align-self-center">
                                                            <p className="h5">{element.name}</p>
                                                            <p className="">Club Limit: <span
                                                                className="font-weight-bold">{element.member_limit}</span>
                                                            </p>
                                                            <p className="">Member Count: N/A</p>
                                                            <p className="">level {element.level}</p>

                                                        </div>
                                                        <div className="col-4 align-self-center">
                                                            <div className="row">
                                                                <div className="col-12 text-center">
                                                                    {/*<p className="h5">&#8358; {element.amount}</p>*/}
                                                                </div>
                                                                <div className="col-12 align-self-end text-center">
                                                                    <div className="action_options">
                                                                        <Dropdown>

                                                                            <Dropdown.Toggle
                                                                                variant="dark"
                                                                                size="sm"
                                                                                id="dropdown-basic">
                                                                                Action
                                                                            </Dropdown.Toggle>

                                                                            <Dropdown.Menu>
                                                                                <Dropdown.Item>
                                                                                    <Link
                                                                                        to={routeList.club.view_club_members + "/" + element.id + "/" + element.name}
                                                                                        style={{ color: 'inherit', textDecoration: 'inherit'}}
                                                                                    >
                                                                                        View Members
                                                                                    </Link>
                                                                                </Dropdown.Item>
                                                                                <Dropdown.Item>
                                                                                    <Link
                                                                                        to={routeList.club.update_club + "/" + element.id + "/" + element.name}
                                                                                        style={{ color: 'inherit', textDecoration: 'inherit'}}
                                                                                    >
                                                                                        Edit Club
                                                                                    </Link>
                                                                                </Dropdown.Item>
                                                                                <ViewClubDetailsModal club_details ={element}/>
                                                                            </Dropdown.Menu>
                                                                        </Dropdown>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    )
                    }
                </div>
            </div>
        );
    }

}

export default AddClientPage;
