import React, {useState} from 'react';
import {Badge, Button, Card, ListGroup} from 'react-bootstrap';
import {Modal} from 'react-bootstrap';
import {Dropdown} from "react-bootstrap";
import {Container} from "react-bootstrap";
import {Row} from "react-bootstrap";
import {Col} from "react-bootstrap";
import {Link} from "react-router-dom";

import {routeList} from "../../config/routeList";


export function ViewClubDetailsModal(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    console.log(props);

    return (
        <>
            <Dropdown.Item onClick={handleShow}> View Club details </Dropdown.Item>

            <Modal
                size="lg"
                show={show}
                onHide={handleClose}
                scrollable={true}
            >

                <Modal.Header closeButton>
                    <Modal.Title>
                    <span
                        className="text-info"> {props.club_details.name} </span> DETAILS
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col xs={12} md={6}>
                                <div className="mt-4">
                                    <Card border="secondary" bg="secondary">
                                        <Card.Body>
                                            <ListGroup>
                                                <ListGroup.Item>
                                                    <b>Club name:</b>
                                                    <b>
                                                        <span className="float-right">
                                                            <Badge pill variant="dark">
                                                                {props.club_details.name}
                                                             </Badge>
                                                        </span>
                                                    </b>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <b>Club level:</b>
                                                    <b>
                                                        <span className="float-right">
                                                            <Badge pill variant="dark">
                                                                {props.club_details.level}
                                                             </Badge>
                                                        </span>
                                                    </b>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <b>Member limit:</b>
                                                    <b>
                                                        <span className="float-right">
                                                            <Badge pill variant="dark">
                                                               {props.club_details.member_limit}
                                                             </Badge>
                                                        </span>
                                                    </b>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <b>Referral target:</b>
                                                    <b>
                                                        <span className="float-right">
                                                            <Badge pill variant="dark">
                                                                {props.club_details.club_target.club_referral_target}
                                                             </Badge>
                                                        </span>
                                                    </b>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <b>Client target:</b>
                                                    <b>
                                                        <span className="float-right">
                                                            <Badge pill variant="dark">
                                                                {props.club_details.club_target.club_client_target}
                                                             </Badge>
                                                        </span>
                                                    </b>
                                                </ListGroup.Item>
                                            </ListGroup>
                                        </Card.Body>
                                    </Card>
                                </div>
                            </Col>
                            <Col xs={12} md={6}>
                                <div className="mt-4">
                                    <Card border="secondary" bg="secondary">
                                        <Card.Body>
                                            <div className="form-group">
                                                <img src={props.club_details.image}
                                                     className="rounded-circle img-fluid mx-auto d-block border"
                                                     alt="Cinque Terre"/>
                                            </div>
                                        </Card.Body>
                                    </Card>
                                </div>
                            </Col>
                            <Col xs={12} md={12}>
                                <div className="mt-4">
                                    <Card border="secondary" bg="secondary">
                                        <Card.Header>
                                            <b>About club</b>
                                        </Card.Header>
                                        <Card.Body>
                                            <div className="form-group">
                                                {props.club_details.requirement_details}
                                            </div>
                                        </Card.Body>
                                    </Card>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Link className="btn btn-dark"
                          to={routeList.club.update_club + "/" + props.club_details.id + "/" + props.club_details.name}>
                        Edit Club
                    </Link>
                    <Button variant="danger" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
