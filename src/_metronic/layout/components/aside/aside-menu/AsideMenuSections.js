import React from "react";
import { NavLink } from "react-router-dom";
import { useLocation } from "react-router";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import PropTypes from "prop-types";

export function AsideMenuSection({
  sectionHeader,
  sectionRoute,
  sectionTitle,
  sectionName,
  sectionSVG,
  sectionLevels,
}) {
  const location = useLocation();

  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>


      {/* section Route and section Title*/}
      <li
        className={`menu-item menu-item-submenu ${getMenuItemActive(
          { sectionRoute },
          true
        )}`}
        aria-haspopup="true"
        data-menu-toggle="hover"
      >
        <NavLink className="menu-link menu-toggle" to="/google-material">
          <span className="svg-icon menu-icon">
            <SVG src={toAbsoluteUrl(sectionSVG)} />
          </span>
          <span className="menu-text">{sectionTitle}</span>
          <i className="menu-arrow" />
        </NavLink>

        <div className="menu-submenu ">
          <i className="menu-arrow" />
          <ul className="menu-subnav">
            <li className="menu-item  menu-item-parent" aria-haspopup="true">
              <span className="menu-link">
                <span className="menu-text">{sectionName}</span>
              </span>
            </li>

            {/* section levels*/}
            {sectionLevels.map((value, i) => {
              return (
                <li
                  key={i}
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    value.route
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to={value.route}>
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">{value.levelName}</span>
                  </NavLink>
                </li>
              );
            })}
          </ul>
        </div>
      </li>
    </>
  );
}

AsideMenuSection.propTypes = {
  sectionHeader: PropTypes.string,
  sectionRoute: PropTypes.string,
  sectionTitle: PropTypes.string,
  sectionName: PropTypes.string,
  sectionSVG: PropTypes.string,
  sectionLevels: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      route: PropTypes.string,
      levelName: PropTypes.string,
    })
  ),
};
