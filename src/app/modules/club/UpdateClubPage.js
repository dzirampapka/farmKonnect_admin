import React, {Component} from "react";

import {submit} from "send-form";

import {apiAction} from "../../helpers/apiConnection";
import {api} from "../../config/apiList";
import {routeList} from "../../config/routeList";
import {notify} from "../../helpers/genericFunctions";
import {
    Card,
    Spinner
} from "react-bootstrap";
import { fromPairs } from "lodash";

class UpdateClubPage extends Component {
    state = {
        club_name: this.props.match.params.club_name,

        //hide and show logo form element
        change_logo_button: " d-inline",
        change_logo_form: " d-none",

        //notification for user actions
        message: {},
        loader: false,

        //initial data for form, to prevent error
        data: {
            name: null,
            member_limit: null,
            requirement_details: "",
            image: null,
            club_target: {
                club_referral_target: null,
                club_client_target: null
            }
        },
        image: null,
    };

    componentDidMount() {


        //parameters passed
        let club_id = this.props.match.params.club_id;


        //append club_id to get_club api
        let get_club_api = {...api.club.get_club};
        get_club_api.url = get_club_api.url + club_id;


        apiAction(get_club_api, this.init_data);


        //show logo form
        let change_logo_button = document.getElementById("change_logo_button");
        change_logo_button.addEventListener('click', this.show_logo_form);

        //hide logo form
        let cancel_logo_change = document.getElementById("cancel_logo_change");
        cancel_logo_change.addEventListener('click', this.hide_logo_form);

    }

    show_logo_form = () => {
        this.setState({
            change_logo_form: " d-block",
            change_logo_button: " d-none"
        })
    }

    hide_logo_form = () => {
        this.setState({
            change_logo_form: " d-none",
            change_logo_button: " d-inline"

        })
    }

    init_data = (res) => {
        let data = res.data.data[0];

        if (res.data.data[0] !== undefined) {
            this.setState({
                data,
                image: data.image
            }, () => console.log(this.state));
        }

    };

    render() {
        let handleSubmit = (e) => {
            e.preventDefault();
            notify("Please wait", "default");


            let update_club_api = {...api.club.update_club};
            update_club_api.url = update_club_api.url + this.props.match.params.club_id;

            submit(club_call_back_fn, "update_club", update_club_api);
        };
        let club_call_back_fn = (res) => {
            console.log(res);
            if (res.status === 200) {
                // this.props.history.push(routeList.club.view_clubs);
                notify("Club details updated successfully", "");
            }
        };


        let handle_logo_submit = (e) => {
            e.preventDefault();
            notify("Please wait", "default");

            let update_club_logo_api = {...api.club.update_club_logo};
            update_club_logo_api.url = update_club_logo_api.url + this.props.match.params.club_id;

            // generic_submit(logo_call_back_fn, "update_club_logo", update_club_logo_api);
            submit(logo_call_back_fn, "update_club_logo", update_club_logo_api);

        };

        let logo_call_back_fn = (res) => {
            if (res.status === 200) {
                // this.props.history.push(routeList.club.view_clubs);
                notify("Club logo updated successfully", "default");

                this.setState({
                    image: res.data.data[0].image,
                    change_logo_form: " d-none",
                    change_logo_button: " d-inline"
                })
            }
        };


        return (
            <div className="page-container mt-3">
                <div className="main-content">
                    <div className="row justify-content-md-center">

                        <div className="col-md-7 mb-5">
                            <div className="card">
                                <div className="card-body">
                                    <div className="text-center mb-10 mb-lg-20">
                                        <h3 className="font-size-h1">EDIT <span
                                            className="text-info"> {this.state.club_name} </span> DETAILS</h3>
                                        <p className="text-muted font-weight-bold">
                                            Enter Club Details
                                        </p>
                                    </div>


                                    <form
                                        id="update_club"
                                        onSubmit={handleSubmit}
                                        encType="multipart/form-data"
                                    >
                                        <div className="form-group">
                                            <label
                                                className="font-weight-semibold"
                                                htmlFor="userName"
                                            >
                                                Club Name:
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="name"
                                                placeholder="Club Name"
                                                defaultValue={this.state.data.name}

                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Member Limit:
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="member_limit"
                                                placeholder="Number of Members Allowed"
                                                defaultValue={this.state.data.member_limit}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Referral Target:
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="club_referral_target"
                                                placeholder="Number of Members Allowed"
                                                defaultValue={this.state.data.club_target.club_referral_target}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Client Target:
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="club_client_target"
                                                placeholder="Number of Members Allowed"
                                                defaultValue={this.state.data.club_target.club_client_target}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="font-weight-semibold" htmlFor="email">
                                                Club Requirements:
                                            </label>
                                            <textarea
                                                className="form-control form-control-solid h-auto py-5 px-6"
                                                name="requirement_details"
                                                placeholder="Enter Club requirements"
                                                defaultValue={this.state.data.requirement_details}
                                            >


                                            </textarea>
                                        </div>
                                        <div className="form-group">
                                            <div className="text-center p-t-15">
                                                <button className="btn btn-rounded btn-dark">
                                                    Update Club Details
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-body">
                                    <div className="text-center mb-10 mb-lg-20">
                                        <h3 className="font-size-h1">CHANGE CLUB LOGO</h3>
                                    </div>

                                    <form
                                        id="update_club_logo"
                                        onSubmit={handle_logo_submit}
                                        encType="multipart/form-data"
                                    >
                                        <div className="form-group">
                                            <img src={this.state.image} className="rounded-circle img-fluid mx-auto d-block border"
                                                 alt="Cinque Terre"/>
                                        </div>
                                        <div className="form-group">
                                            <div className={"text-center p-t-15"}>
                                                <div
                                                    className={"btn btn-rounded btn-dark" + this.state.change_logo_button}
                                                    id="change_logo_button"
                                                >
                                                    Change Logo
                                                    <i className="fa fa-pencil" aria-hidden="true"> </i>
                                                </div>
                                            </div>
                                        </div>

                                        <div className={this.state.change_logo_form}>
                                            <div className="form-group  mt-5">
                                                <label
                                                    className="font-weight-semibold"
                                                    htmlFor="confirmPassword"
                                                >
                                                    Club Logo
                                                </label>
                                                <input
                                                    type="file"
                                                    className="form-control form-control-solid h-auto py-5 px-6"
                                                    id="club_logo"
                                                    name="image"
                                                    required
                                                />
                                            </div>

                                            <div className="form-group">
                                                <div className="text-center p-t-15">
                                                    <div className="btn btn-rounded btn-danger mr-5"
                                                         id="cancel_logo_change">
                                                        Cancel
                                                        <i className="fa fa-pencil" aria-hidden="true"> </i>
                                                    </div>
                                                    <button className="btn btn-rounded btn-dark ml-5">
                                                        Save
                                                        <i className="fa fa-pencil" aria-hidden="true"> </i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UpdateClubPage;
