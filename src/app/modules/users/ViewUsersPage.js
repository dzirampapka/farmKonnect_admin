import React, {Component} from "react";
import {Dropdown, Badge} from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from "react-bootstrap-table2-paginator";
import {api} from "../../config/apiList";
import {InPageLoader} from "../../../_metronic/layout/components/loader/inPageLoader";
import {apiAction} from "../../helpers/apiConnection";
import {UserDetailsModal} from "./userDetailsModal";
import {AdminActionConfirmationModal} from "../users/adminActionConfirmationModal";
import {notify} from "../../helpers/genericFunctions";



class ViewUsersPage extends Component {

    app_status = (cell, row) => {

        let status_word = "";
        let badge_status = "";

        if (row !== undefined) {
            if (row.hasOwnProperty('status')) {

                if (parseInt(row.status) === 0) {

                    status_word = "Not Activated";
                    badge_status = "warning";

                } else if (parseInt(row.status) === 1) {

                    status_word = "Activated";
                    badge_status = "primary";

                } else if (parseInt(row.status) === 2) {

                    status_word = "Verified";
                    badge_status = "success";

                } else if (parseInt(row.status) === 3) {

                    status_word = "Suspended";
                    badge_status = "danger";

                }
            }
        }

        return (

            <div className="btn btn-outline btn-rounded button btn-block">
                <Badge pill variant={badge_status}>{status_word}</Badge>
            </div>

        )
    };

    table_image = function (cell, row, rowIndex) {
        // let passport_image = "assets/images/user_placeholder.png";
        if (row !== undefined) {
            if (row.hasOwnProperty("passport")) {

                let passport_image = row.passport ? row.passport : "assets/images/user_placeholder.png";
                return (
                    <div>
                        <div className="row">
                            <div className="col-3">
                                <div className="avatar avatar-image">
                                    <img src={passport_image} className="rounded-circle table_image" alt=""/>
                                </div>
                            </div>
                            <div className="col-9 pt-2">
                                <div>
                                    <span className="mr-2"> {row.name} </span>
                                </div>
                            </div>
                        </div>
                    </div>

                )
            }
        }

    };

    init_status = (res) => {

        // console.log(res.data.data);

        let new_status = res.data.data.status;

        //if api call is successful
        if (res.status === 200) {
            notify("Successful", "default");

            let user_data = this.props.users.map((element, index) => {

                //match row id
                if (parseInt(res.data.data.id) === parseInt(element.id)) {
                    element.status = new_status;
                }
                return element;
            });

            //update parent state...which propagates to child components
            //Random figure was sent to parent in other to trigger re-rendering
            this.props.update_status(user_data);

            // console.log(this.props.status);

        }

    };

    my_action = (api_dir, row) => {

        notify("Please wait", "default");

        let axios_header = {...api_dir};
        axios_header.url = axios_header.url + row.id;
        apiAction(axios_header, this.init_status);
    };

    button_action = (cell, row, rowIndex) => {

        let options = [];

        //check if status exist to avoid error
        if (row !== undefined) {
            if (row.hasOwnProperty('status')) {
                if (parseInt(row.status) === 1) {
                    options.push(
                        {action_api: api.admin_actions.suspend_affiliate, row: row, word: " Suspend"},
                        {action_api: api.admin_actions.verify_affiliate, row: row, word: " Verify"},
                    )
                } else if (parseInt(row.status) === 2) {
                    options.push(
                        {action_api: api.admin_actions.suspend_affiliate, row: row, word: " Suspend"},
                        {action_api: api.admin_actions.unverify_affiliate, row: row, word: " Unverify"},
                    )
                } else if (parseInt(row.status) === 3) {
                    options.push(
                        {action_api: api.admin_actions.unsuspend_affiliate, row: row, word: " Unsuspend"},
                    )
                }
            }
        }


        return (
            <div className="action_options">
                <Dropdown>
                    <Dropdown.Toggle variant="dark" id="dropdown-basic">
                        Action
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        {
                            options.map((element) => {
                                return (
                                    <AdminActionConfirmationModal
                                        action={() => this.my_action(element.action_api, element.row)}
                                        action_title = {element.word}
                                        row_data = {element.row}
                                    />
                                )
                            })
                        }
                        <UserDetailsModal/>

                    </Dropdown.Menu>
                </Dropdown>
            </div>
        )
    };

    render() {
        const columns = [
            {
                dataField: 'name',
                text: 'Name',
                sort: true,
                align: 'center',
                formatter: this.table_image,
                headerAlign: 'center',
            }, {
                dataField: 'club_name',
                text: 'Club',
                align: 'center',
                sort: true,
                headerAlign: 'center'
            }, {
                dataField: 'phone',
                text: 'Phone',
                align: 'center',
                sort: true,
                headerAlign: 'center'
            }, {
                dataField: 'email',
                text: 'Email',
                align: 'center',
                sort: true,
                headerAlign: 'center'
            }, {
                dataField: 'status',
                text: 'Status',
                align: 'center',
                formatter: this.app_status,
                sort: true,
                headerAlign: 'center'
            }, {
                dataField: 'passport',
                text: 'Action',
                align: 'center',
                formatter: this.button_action,
                headerAlign: 'center',
                isDummyField: true
            }];

        const defaultSorted = [{
            dataField: 'name',
            order: 'desc'
        }];

        const {loader} = this.props;

        return (
            <div key={this.props.status} className="page-container">
                <div className="main-content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="row">
                                    <div className="col-lg-12 mb-3">
                                        <div className="card-header text-center bg-light">
                                            <span
                                                className="h5 text-dark-50 text-uppercase">{this.props.table_title}</span>
                                        </div>
                                    </div>
                                </div>


                                <div className="card-body">
                                    <InPageLoader loader={loader}/>

                                    <BootstrapTable
                                        keyField='id'
                                        data={this.props.users === null ? [] : this.props.users}
                                        columns={columns}
                                        bootstrap4
                                        striped
                                        classes="table table-head-custom table-vertical-center overflow-hidden"
                                        defaultSorted={defaultSorted}
                                        pagination={paginationFactory()}
                                        bordered={false}
                                        search
                                        wrapperClasses="table-responsive"

                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ViewUsersPage;
