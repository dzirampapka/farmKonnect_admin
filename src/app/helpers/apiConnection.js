import axios from "axios";


export const reduxApiAction = (dispatch, api, action_type, id) => {
    axios({
        method: api.method,
        url: api.url,
        headers: api.headers,
        data: api.data,
    }).then(res => {
        dispatch({
            type: action_type,
            payload: id
        });
    });
};

export const apiAction = async (api, callback) => {
    await axios({
        method: api.method,
        url: api.url,
        headers: api.headers,
        data: api.data,
    }).then(res => {
        callback(res);
    }).catch(err => {
        callback(err);
    })
};
